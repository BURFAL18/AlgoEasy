import React, { Component } from "react";
import { Router, Route, Redirect } from "react-router-dom";
import Select from "./components/select";
import Sorting from "./components/AlgoVisualiser/sorting/sorting";
import Pathfinding from "./components/AlgoVisualiser/pathfinder/pathfinding";
import Popup from "./components/popup/popup";
import history from "./components/history";

class App extends Component {
  state = { hidden: false };

  disableLinks = () => {
    if (this.state.hidden === false) {
      this.setState({ hidden: true });
    } else {
      this.setState({ hidden: false });
    }
  };

  render() {
    return (
      <>
        <Router history={history}>
          <Select hidden={this.state.hidden} />
          <Route render={() => <Redirect to="/pathfinding/welcome" />} />
          <Route
            path="/sorting"
            render={(routeProps) => (
              <Sorting {...routeProps} disableLinks={this.disableLinks} />
            )}
          />
          <Route
            path="/sorting/welcome"
            render={(routeProps) => (
              <Popup
                {...routeProps}
                styling={"algorithms"}
                header={"Welcome!"}
                content={"Select an Algorithm & click start."}
                linkTo={"/sorting"}
                onDismiss={() => history.push("/sorting")}
              />
            )}
          />
          <Route
            path="/pathfinding"
            render={(routeProps) => (
              <Pathfinding {...routeProps} disableLinks={this.disableLinks} />
            )}
          />
          <Route
            path="/pathfinding/welcome"
            render={(routeProps) => (
              <Popup
                {...routeProps}
                styling={"welcome"}
                header={"Welcome!"}
                content={
                  "Create Wall or set Start/Finish by clicking or dragging."
                }
                linkTo={"/pathfinding"}
                onDismiss={() => history.push("/pathfinding")}
              />
            )}
          />
          <Route
            path="/pathfinding/warning"
            render={(routeProps) => (
              <Popup
                {...routeProps}
                styling={"warning"}
                header={"Sorry!"}
                content={":( No path found"}
                linkTo={"/pathfinding"}
                onDismiss={() => history.push("/pathfinding")}
              />
            )}
          />
        </Router>
      </>
    );
  }
}

export default App;
