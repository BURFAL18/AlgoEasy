![ReactJs](https://user-images.githubusercontent.com/56060354/97405855-53c40280-191e-11eb-8fe5-8d7878b0b280.png) 
![Author](https://img.shields.io/badge/author-Brijesh%20Burfal-lightgrey.svg?colorB=9900cc&style=flat-square)
## AlgoEasy - Visualiser for Algo
Try it Now 
#### https://burfal18.github.io/AlgoEasy
### This application visualises Sorting Algos and Dijsktra Algo . Which helps user understand the algorithm better.

### Version 1 FEATURES

* Sorting Algorithms
  * Bubble Sort
  * Heap Sort
  * Merge Sort
  * Quick Sort

  ### SAMPLE SORT ALGO 
 ![image](https://user-images.githubusercontent.com/56060354/127001705-6b6298e9-f2c7-4441-9dc0-1a8a65881e8c.png)
 
 ### AFTER SORTING
![image](https://user-images.githubusercontent.com/56060354/127001799-3eb483eb-4795-4389-a907-cc26ef3ff9f2.png)

* Path Finder
  * Dijsktra Algorithm
![image](https://user-images.githubusercontent.com/56060354/127001573-9171f548-4c2a-42e2-badb-092b77b7f315.png)


### Tech Stack
AlgoEasy uses these tech:

* [ReactJs] - HTML enhanced library for web apps!
* [Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend


   [git-repo-url]: <https://github.com/BURFAL18/AlgoEasy>
   [node.js]: <http://nodejs.org>
   [ReactJs]: <http://reactjs.org>
   [BootStrap]:<https://getbootstrap.com/docs/4.0>
